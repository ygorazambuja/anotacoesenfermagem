package br.ufms.AnotacoesEnfermagem.domain.exception;

public class GenericSqlRuntimeException extends Throwable {
    public GenericSqlRuntimeException(String s, Exception e) {
        super(s, e);
    }

    public GenericSqlRuntimeException(String s) {
        super(s);
    }

    public GenericSqlRuntimeException(Exception e) {
        super(e);
    }
}
