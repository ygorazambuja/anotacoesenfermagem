package br.ufms.AnotacoesEnfermagem.domain.exception;

public class GenericSqlInexistenteRuntimeException extends RuntimeException {
    public GenericSqlInexistenteRuntimeException(String s) {
        super(s);
    }
}
