package br.ufms.AnotacoesEnfermagem.domain.exception;

public class GenericRuntimeException extends RuntimeException {
    public GenericRuntimeException(String s) {
        super(s);
    }
}
