package br.ufms.AnotacoesEnfermagem.domain.enumerations;

public enum ETipoDocumento {

//    todo -  instanciar new document no construtor  do enum

    ADMISSAO("Admissão"),
    ALTA("Alta"),
    ADMINISTRACAO_DE_MEDICAMENTOS("Administração de medicamentos"),
    ASPIRACAO_ORAL("Aspiração oral"),
    ASPIRACAO_TRAQUEAL("Aspiração traqueal (Enfermeiro)"),
    ACESSO_VENOSO("Acesso venoso"),
    AVALIACAO_NIVEL_CONSCIENCIA_PELO_ENFERMEIRO("Avaliação do nível de consciência pelo enfermeiro"),
    AUXILIO_NA_DIETA("Auxílio na dieta"),
    BANHO_DE_ASSENTO("Banho de assento"),
    BALANCO_HIDROELETROLITICO_PELO_ENFERMEIRO("Balanço hidroeletrolítico pelo enfermeiro"),
//    Entrada de líquidos,
//    Saída de líquidos
    COLETA_DE_LINFA_PARA_HANSENIASE("Coleta de linfa para hanseníase"),
    COLETA_MATERIAL_PARA_TESTE_PEZINHO("Coleta de material para o teste do pezinho"),
    COLETA_EXAME_CITOPATOLOGICO_PELO_ENFERMEIRO("Coleta de exame citopatológico pelo enfermeiro"),
    CONDUTAS_DE_SEGURANCA_AO_PACIENTE("Condutas de segurança ao paciente"),
    Consulta_de_enfermagem_peloenfermeiro("Consulta de enfermagem pelo enfermeiro"),
    CONTENCAO_NO_LEITO("Contenção no leito"),
    CONTROLE_DA_DOR("Controle da dor"),
    CONTROLE_HIDRICO("Controle hídrico"),
//    Entrada de líquidos,
//    Saída de líquidos
    CLASSIFICACAO_DE_RISCO("Classificação de risco"),
    CURATIVOS("Curativos"),
    CUIDADOS_COM_ESTOMAS("Cuidados com estomas"),
    CUIDADOS_COM_OS_PES("Cuidados com os pés"),
    CUIDADOS_IMEDIATOS_COM_RN("Cuidados Imediatos com o RN (Enfermeiro)"),
    CUIDADOS_COM_RN_EM_FOTOTERAPIA("Cuidados com o RN em fototerapia"),
    CUIDADOS_PRE_PARTO("Cuidados no pré-parto"),
    CUIDADOS_SALA_DE_PARTO("Cuidados na sala de parto"),
    CUIDADOS_NO_POS_PARTO_IMEDIATO("Cuidados no pós-parto imediato"),
    DRENAGEM_DE_TORAX("Drenagem de tórax (Enfermeiro)"),
    DRENOS("Drenos"),
    DIALISE_PERITONEAL("Diálise peritoneal"),
    ENCAMINHAMENTO_EXAMES("Encaminhamento para exames, Centro Cirúrgico e Centro Obstétrico"),
//    Centro Cirúrgico e Centro Obstétrico
    Enteróclise("Enteróclise"),
    EXAME_DE_MONTENEGRO("Exame de Montenegro"),
    GLICEMIA_CAPILAR("Glicemia capilar"),
    HEMODIALISE("Hemodiálise"),
    HIGIENE_DO_PACIENTE_BANHO("Higiene do paciente–banho"),
    HIGIENE_DO_COURO_CABELUDO("Higiene do couro cabeludo"),
    HIGIENE_INTIMA("Higiene íntima"),
    HIGIENE_ORAL("Higiene oral"),
    IMOBILIZACAO("Imobilização"),
    IRRIGACAO_DE_SONDA_VESICAL_E_BEXIGA("Irrigação de sonda vesical e bexiga"),
    INALACAO_NEBULIZACAO("Inalação / nebulização"),
    LAVADO_GASTRICO("Lavado gástrico"),
    MASSAGEM_DE_CONFORTO("Massagem de conforto"),
    MEDIDA_ANTROPOMETRICA("Medida antropométrica"),
    MUDANÇA_DE_DECUBITO("Mudança de decúbito"),
    NUTRICAO_ENTERAL("Nutrição enteral"),
    NUTRICAO_PARENTERAL_PELO_ENFERMEIRO("Nutrição parenteral pelo enfermeiro"),
    OBITO("Óbito"),
    ORDENHA_MAMARIA("Ordenha mamária"),
    PUNCAO_ARTERIAL_PELO_ENFERMEIRO("Punção arterial pelo enfermeiro"),
    PREPARO_PSICOLOGICO_DO_CLIENTE_PACIENTE("Preparo psicológico do cliente / paciente"),
    PRESSAO_VENOSA_CENTRAL_PVC_PELO_ENFERMEIRO("Pressão venosa central (PVC) pelo enfermeiro"),
    PRESSAO_ARTERIAL_MEDIA_PAM_PELO_ENFERMEIRO("Pressão arterial média (PAM) pelo enfermeiro"),
    PRE_OPERATORIO("Pré-operatório"),
    POS_OPERATORIO_IMEDIATO("Pós-operatório imediato"),
    POS_OPERATORIO_MEDIATO("Pós-operatório mediato"),
    PROVA_DO_LACO("Prova do laço"),
    REGISTROS_RELATIVOS_A_COLETA_DE_MATERIAL_PARA_EXAMES("Registros relativos à coleta de material para exames"),
    REGISTROS_RELATIVOS_A_DEAMBULACAO("Registros relativos à deambulação"),
    RETIRADA_DE_PONTOS("Retirada de pontos"),
    SINAIS_VITAIS("Sinais vitais"),
    TESTE_DE_PPD("Teste de PPD"),
    TESTE_DE_GRAVIDEZ("Teste de gravidez"),
    TESTE_RAPIDO_DE_HIV("Teste rápido de HIV"),
    TESTE_RAPIDO_PARA_SIFILIS("Teste rápido para Sífilis"),
    TESTE_PARA_HEPATITE("Teste para Hepatite"),
    TERAPIA_DE_REIDRATACAO_ORAL_TRO("Terapia de Reidratação Oral (TRO)"),
    TRANSFERENCIA_INTERNA("Transferência interna"),
    TRANSFERENCIA_EXTERNA("Transferência externa"),
    TRATAMENTO_DE_PEDICULOSE("Tratamento de pediculose"),
    TRATAMENTO_DE_MIIASE("Tratamento de miíase"),
    TRANSOPERATORIO("Transoperatório"),
    TRICOTOMIA("Tricotomia"),
    VACINA("Vacina");

    private String tipoDocumento;

    ETipoDocumento(String tipoDoc){
        this.tipoDocumento = tipoDoc;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }
}
