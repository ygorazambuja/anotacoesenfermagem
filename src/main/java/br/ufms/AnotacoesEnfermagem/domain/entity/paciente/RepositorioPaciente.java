package br.ufms.AnotacoesEnfermagem.domain.entity.paciente;

import java.util.List;

public interface RepositorioPaciente {

    Paciente buscarPorId(Long idPaciente);

    List<Paciente> buscarTodosPacientes();

    void salvarPaciente(Paciente paciente);

    void excluirPacientePorId(Long idPaciente);

    Paciente editarPaciente(Paciente paciente);

    Paciente buscarPacientePorCPF(String cpf);
}
