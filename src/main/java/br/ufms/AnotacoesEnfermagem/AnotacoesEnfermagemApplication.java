package br.ufms.AnotacoesEnfermagem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnotacoesEnfermagemApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnotacoesEnfermagemApplication.class, args);
	}

}
