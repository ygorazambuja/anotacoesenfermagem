package br.ufms.AnotacoesEnfermagem.infra.repository.jpa;

import br.ufms.AnotacoesEnfermagem.domain.entity.paciente.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacienteRepository  extends JpaRepository<Paciente, Long> {
}
