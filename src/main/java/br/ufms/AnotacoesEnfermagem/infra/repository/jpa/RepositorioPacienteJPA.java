package br.ufms.AnotacoesEnfermagem.infra.repository.jpa;

import br.ufms.AnotacoesEnfermagem.domain.entity.paciente.Paciente;
import br.ufms.AnotacoesEnfermagem.domain.entity.paciente.RepositorioPaciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepositorioPacienteJPA implements RepositorioPaciente {
    
    @Autowired
    private PacienteRepository repositorio;
    
    @Override
    public Paciente buscarPorId(Long idPaciente) {
        return repositorio.findById(idPaciente).stream().findFirst().orElse(null);
    }

    @Override
    public List<Paciente> buscarTodosPacientes() {
        return null;
    }

    @Override
    public void salvarPaciente(Paciente paciente) {

    }

    @Override
    public void excluirPacientePorId(Long idPaciente) {

    }

    @Override
    public Paciente editarPaciente(Paciente paciente) {
        return null;
    }

    @Override
    public Paciente buscarPacientePorCPF(String cpf) {
        return null;
    }


}
