package br.ufms.AnotacoesEnfermagem.application.controller;

import br.ufms.AnotacoesEnfermagem.application.service.GenericService;
import br.ufms.AnotacoesEnfermagem.domain.exception.GenericSqlRuntimeException;
import br.ufms.AnotacoesEnfermagem.domain.generic.GenericEntity;
import br.ufms.AnotacoesEnfermagem.domain.generic.GenericRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.List;

@MappedSuperclass
@CrossOrigin
public class GenericController <
        C extends GenericService<E, R, I>,
        E extends GenericEntity<I>,
        R extends GenericRepository<E, I>,
        I extends Serializable> {

    @Autowired
    private C business;

    @Autowired
    private R repository;

    @GetMapping
    @CrossOrigin
    public List<E> buscarTodos() {
        return repository.findAll();
    }

    @GetMapping(path = "/{id}")
    @CrossOrigin
    public Object buscarPorId(@PathVariable("id") I id) {
        return repository.findById(id);
    }

    @PostMapping
    @CrossOrigin
    public E inserir(@RequestBody E entitySalvar) throws GenericSqlRuntimeException {
        return business.inserirComValidacao(entitySalvar);
    }

    @PutMapping(path = "/{id}")
    @CrossOrigin
    public E editar(@RequestBody E entityUpdate, @PathVariable("id") I id) throws GenericSqlRuntimeException {
            return business.editarComValidacao(entityUpdate, id);
    }

    @DeleteMapping(path = "/{id}")
    @CrossOrigin
    public void excluir(@PathVariable("id") I id) throws GenericSqlRuntimeException {
        business.excluir(id);
    }
}

